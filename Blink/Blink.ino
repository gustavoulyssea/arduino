// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT); 
  pinMode(6, INPUT); 
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
  int ativo = digitalRead(5);
  int button;
  button = digitalRead(6);
  if(button==HIGH){
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    if(ativo==1){
      Serial.println("desligando");
      ativo=0;
    }
    else{
      Serial.println("ligando");
      ativo=1;
    }
  }
  if(ativo==1){
    
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);  
    delay(30);
  
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    delay(30);
  
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    delay(30);

    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    delay(30);
  }
  else{
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
  }
  digitalWrite(LED_BUILTIN, LOW);   
}
