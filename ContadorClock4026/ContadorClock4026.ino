void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(2, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(2, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(2, HIGH);
    delay(500);
}
