/* byte Hour;
 byte Minute;
 byte Second;
 byte Day;
 byte DayofWeek; // Sunday is day 0 
 byte Month;     // Jan is month 0
 byte Year;      // the Year minus 1900 

void sync(time_t time);
a method to set the clock to an external time reference, this is usually done by receiving the time over the serial port but can also use other references such as WWV/DCF radio signals.

time_t now();
return the current time as seconds since midnight Jan 1 1970 (aka unix time)

boolean available();
refreshes the Date and Time properties. This returns true if the clock is synced, otherwise it returns false. This method should always be called just before using the time properties because the date and time properties are updated by this call.
Methods to convert to and from time components (hrs, secs, days, years etc) to time_t

void localTime(time_t *timep,byte *psec,byte *pmin,byte *phour,byte *pday,byte *pwday,byte *pmonth,byte *pyear);
extracts time components from time_t. Note that year here is the desired year minus 1900

time_t makeTime(byte sec, byte min, byte hour, byte day, byte month, int year );
returns time_t from time components, note that year here is full four digit year.
 
 */

#include <DateTime.h>
#include <DateTimeStrings.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); 
}

void loop() {
  getPCtime();
  if(DateTime.available()) { // update clocks if time has been synced
    unsigned long prevtime = DateTime.now(); 
    while( prevtime == DateTime.now() )  // wait for the second to rollover
      ;
    DateTime.available(); //refresh the Date and time properties
    digitalClockDisplay( );   // update digital clock
    Serial.println(DateTime.now());      
  } 
}

void getPCtime() {
  // if time available from serial port, sync the DateTime library
  while(Serial.available() >=  TIME_MSG_LEN ){  // time message
    if( Serial.read() == TIME_HEADER ) {        
      time_t pctime = 0;
      for(int i=0; i < TIME_MSG_LEN -1; i++){   
        char c= Serial.read();          
        if( c >= '0' && c <= '9')   
          pctime = (10 * pctime) + (c - '0') ; // convert digits to a number            
      }   
      DateTime.sync(pctime);   // Sync DateTime clock to the time received on the serial port
    }  
  }
}

void digitalClockDisplay(){
  // digital clock display of current time
  Serial.print(DateTime.Hour,DEC);  
  printDigits(DateTime.Minute);  
  printDigits(DateTime.Second); 
  Serial.print(" "); 
  Serial.print(DateTimeStrings.dayStr(DateTime.DayofWeek));
  Serial.print(" ");  
  Serial.print(DateTimeStrings.monthStr(DateTime.Month));  
  Serial.print(" ");
  Serial.println(DateTime.Day, DEC);   
}

void printDigits(byte digits){
  // utility function for digital clock display: prints colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits,DEC);   
}
