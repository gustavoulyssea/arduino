//#include <LiquidCrystal.h>
#include "dht.h"

#include "RTClib.h" //INCLUSÃO DA BIBLIOTECA

// OLED
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

#define dht_apin A1 // Analog Pin sensor is connected to

//LiquidCrystal lcd(13, 12, 11, 10, 9, 8);

// constants won't change. They're used here to set pin numbers:
const int inputPin = A0;     // the number of the pushbutton pin
const int outputPin =  2;      // the number of the LED pin
const int botaoPin = 3;

dht DHT;

RTC_DS1307 rtc; //OBJETO DO TIPO RTC_DS1307

// OLED
Adafruit_SSD1306 dsp(-1);//cria o objeto do display para i2c 


//DECLARAÇÃO DOS DIAS DA SEMANA
char daysOfTheWeek[7][12] = {"Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"};

// variables will change:
int outputState = 0;
int previousState = 0;
int botaoState = 0;
int previousBotaoState = 0;
int ligado = 1;

void setup() {
  // initialize the LED pin as an output:
  pinMode(outputPin, OUTPUT);
  pinMode(inputPin, INPUT);
  pinMode(botaoPin, INPUT);
  Serial.begin(9600);
//  lcd.begin(16, 2);

////////////
  if (! rtc.begin()) { // SE O RTC NÃO FOR INICIALIZADO, FAZ
    Serial.println("DS1307 não encontrado"); //IMPRIME O TEXTO NO MONITOR SERIAL
    while(1);
  }
  if (! rtc.isrunning()) { //SE RTC NÃO ESTIVER SENDO EXECUTADO, FAZ
    Serial.println("DS1307 rodando!"); //IMPRIME O TEXTO NO MONITOR SERIAL
    //REMOVA O COMENTÁRIO DE UMA DAS LINHAS ABAIXO PARA INSERIR AS INFORMAÇÕES ATUALIZADAS EM SEU RTC
    //rtc.adjust(DateTime(F(__DATE__), F(__TIME__))); //CAPTURA A DATA E HORA EM QUE O SKETCH É COMPILADO
    //rtc.adjust(DateTime(2018, 7, 5, 15, 33, 15)); //(ANO), (MÊS), (DIA), (HORA), (MINUTOS), (SEGUNDOS)
  }
  pinMode(4, INPUT);
////////////

// INICIO OLED
  dsp.begin(SSD1306_SWITCHCAPVCC, 0x3C);//inicia o display com endereco padrao
  dsp.clearDisplay();//limpa a tela
 
 
  dsp.setTextColor(WHITE);//define o texto para branco (no display ficara azul)
 
  dsp.setTextSize(1);//define o tamanho do texto
  dsp.println("Vida de silicio");//escreve na tela a mensagem
 
  dsp.setTextSize(3);
  dsp.println("2018");
  dsp.display();//mostra as alteracoes no display, sem isso nao ira mostrar nada!!
  delay(2000);
  dsp.clearDisplay();

// FIM OLED
}
void loop() {

  previousState = digitalRead(outputPin);
  previousBotaoState = botaoState;

  botaoState = digitalRead(botaoPin);
  outputState = analogRead(inputPin);

// botso liga/desliga
  if(previousBotaoState==LOW&&botaoState==HIGH){
    if(ligado==1){
      ligado = 0;
    }
    else{
      ligado = 1;
    }
  }


  DateTime now = rtc.now();
  if(ligado==1&&(now.hour()<=6||now.hour()>=20)){
    if (outputState >= 200) {
      // turn LAMP on:
      digitalWrite(outputPin, HIGH);
    } else {
      // turn LAMP off:
      digitalWrite(outputPin, LOW);
    }
  }
  else{
    if(digitalRead(outputPin)==HIGH){
      digitalWrite(outputPin, HIGH);
    }
  }

//  }
  
  Serial.print ("Leitura atual do sensor: ");
  Serial.println(outputState);
/*
  // Inicio lcd
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("LUZ ");
  lcd.setCursor(4, 0);
  lcd.print(outputState);
  lcd.setCursor(9, 0);
  if(ligado==1){
    lcd.print("ON");
  }
  else{
    lcd.print("OFF");
  }
*/
/*
  //Rolagem para a esquerda
  for (int posicao = 0; posicao < 3; posicao++)
  {
    lcd.scrollDisplayLeft();
    delay(600);
  }
  //Rolagem para a direita
  for (int posicao = 0; posicao < 6; posicao++)
  {
    lcd.scrollDisplayRight();
    delay(600);
  }
*/
// fim lcd

// Inicio temperatura
  DHT.read11(dht_apin);
    
  Serial.print("Current humidity = ");
  Serial.print(DHT.humidity);
  Serial.print("%  ");
  Serial.print("temperature = ");
  Serial.print(DHT.temperature); 
  Serial.println("C  ");
//  lcd.setCursor(13, 0);
//  lcd.print((int) DHT.temperature);
//  lcd.print("C");
// Fim temperatura

// Inicio RTC Clock
//  DateTime now = rtc.now();
  Serial.print("Data: ");
  Serial.print(now.day(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.year(), DEC);
  Serial.print(" / Dia: ");
  Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
  Serial.print(" / Horas: ");
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();
/*
  lcd.setCursor(0, 1);
  lcd.print(now.hour(), DEC);
  lcd.print(':');
  lcd.print(now.minute(), DEC);
  lcd.print(':');
  lcd.print(now.second(), DEC);
*/  
// Fim RTC Clock
// INICIO OLED
  dsp.clearDisplay();//limpa a tela
  dsp.setCursor(0, 0);
  dsp.setTextColor(WHITE);//define o texto para branco (no display ficara azul)
  dsp.setTextSize(2);//define o tamanho do texto
  dsp.print("LUZ:");//escreve na tela a mensagem
  dsp.println(outputState);
  if(ligado==1){
    dsp.print("ON ");
  }
  else{
    dsp.print("OFF ");
  }
  dsp.print((int) DHT.temperature);
  dsp.println("C");

  dsp.print(now.hour(), DEC);
  dsp.print(':');
  dsp.print(now.minute(), DEC);
  dsp.print(':');
  dsp.println(now.second(), DEC);
  dsp.print("_________");
  dsp.display();
// FIM OLED


  delay(500);
}
