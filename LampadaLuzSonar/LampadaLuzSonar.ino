/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/

// constants won't change. They're used here to set pin numbers:
const int inputPin = A0;     // the number of the pushbutton pin
const int outputPin =  2;      // the number of the LED pin

// variables will change:
int outputState = 0;         // variable for reading the pushbutton status
int previousState = 0;

void setup() {
  // initialize the LED pin as an output:
  pinMode(outputPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(inputPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  // read the state of the pushbutton value:

  previousState = digitalRead(outputPin);
  
  outputState = analogRead(inputPin);
  
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
//  if(previousState==HIGH){
    if (outputState >= 200) {
      // turn LED on:
      digitalWrite(outputPin, HIGH);
    } else {
      // turn LED off:
      digitalWrite(outputPin, LOW);
    }
    delay(2000);
//  }
  Serial.print ("Leitura atual do sensor: ");
  Serial.println(outputState);
}
