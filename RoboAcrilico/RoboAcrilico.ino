#include <Servo.h>
 
Servo base;
Servo garra;
Servo esquerda;
Servo direita;

int posBase; 
int posGarra;
int posEsquerda;
int posDireita;

int valorBotao = 0;
 
void setup ()
{
  pinMode(5, INPUT);
  base.attach(6);
  garra.attach(7);
  garra.write(140);
  esquerda.attach(8);
  esquerda.write(100);
  esquerda.write(60);
  direita.attach(9);
  direita.write(60);
  Serial.begin(9600);
  base.write(0); // Inicia motor posição zero
  delay(1000);
}
 
void loop(){
  valorBotao = digitalRead(5);
  if(valorBotao==HIGH){
    for(posBase = 0; posBase < 150; posBase++){
      base.write(posBase);
      delay(15);
    }
    delay(100);
    // Abaixa
    for(posEsquerda = 60; posEsquerda < 110; posEsquerda++){
      esquerda.write(posEsquerda);
      delay(15);
    }
    delay(100);
    // Fecha garra
    for(posGarra = 140; posGarra < 180; posGarra++){
      garra.write(posGarra);
      delay(15);
    }
    delay(100);
    // Levanta
    for(posEsquerda = 110; posEsquerda >= 60; posEsquerda--){
      esquerda.write(posEsquerda);
      delay(15);
    }
    delay(100);
    // Vira metade
    for(posBase = 150; posBase >= 75; posBase--){
      base.write(posBase);
      delay(15);
    }
    delay(100);
    // Abaixa
    for(posEsquerda = 60; posEsquerda < 110; posEsquerda++){
      esquerda.write(posEsquerda);
      delay(15);
    }
    delay(100);
    // Abre garra
    for(posGarra = 180; posGarra >= 140; posGarra--){
      garra.write(posGarra);
      delay(15);
    }
   delay(100);
    // Levanta
    for(posEsquerda = 110; posEsquerda >= 60; posEsquerda--){
      esquerda.write(posEsquerda);
      delay(15);
    }
    delay(100);
    for(posBase = 75; posBase >= 0; posBase--){
      base.write(posBase);
      delay(15);
    }

  }
}
