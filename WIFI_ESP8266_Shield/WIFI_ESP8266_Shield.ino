#include <SoftwareSerial.h> //INCLUSÃO DE BIBLIOTECA
SoftwareSerial serialESP8266(0, 1); //PINOS QUE EMULAM A SERIAL (PINO 0 É O RX E PINO 1 É O TX)
 
void setup() {
 Serial.begin(9600); //INICIALIZA A SERIAL
 serialESP8266.begin(115200); //INICIALIZA A SOFTWARESERIAL
 serialESP8266.println("AT+UART_DEF");
  delay(1000);
  serialESP8266.end();
  // Start the software serial for communication with the ESP8266
  serialESP8266.begin(9600);
}
 
void loop() {
  Serial.println("a");
 if (serialESP8266.available()) { //SE SOFTWARESERIAL HABILITADA, FAZ
  serialESP8266.println("AT+GMR");
  String msg = serialESP8266.readString(); //VARIÁVEL RECEBE AS INFORMAÇÕES VINDAS PELA SOFTWARESERIAL
  
  Serial.println(msg); //IMPRIME O TEXTO NA SERIAL
 }
}
